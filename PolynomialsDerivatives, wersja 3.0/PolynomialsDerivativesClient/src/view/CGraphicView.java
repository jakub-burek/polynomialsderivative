/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package view;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import static javax.swing.JOptionPane.QUESTION_MESSAGE;
import controler.IController;
import java.util.LinkedList;

/**
 * View's implementation. It can read data, print results and set its current
 * controller.
 *
 * @version 3.0
 * @author jakub
 */
public class CGraphicView implements IView {

    /**
     * No-parameter CView constructor.
     */
    public CGraphicView() {

    }

    /**
     * This method reads data from input dialog. All non-numerical characters
     * are replaced with spaces. Scanner reads doubles from input string and
     * sends them as polynomial's factors to the controller.
     */
    @Override
    public void readData() {
        String FactorsString = JOptionPane.showInputDialog(null, "Type in polynomial's factors in descending order: ", "Polynomial Derivatives", QUESTION_MESSAGE);
        if (FactorsString == null) {
            return;
        }
        FactorsString = FactorsString.replaceAll("[^-.0-9]+", " "); //"[^-?0-9]+"
        Scanner sc = new Scanner(FactorsString);
        sc.useLocale(Locale.US);
        List<Double> FactorsArray = new ArrayList<>();
        while (sc.hasNextDouble()) {
            FactorsArray.add(sc.nextDouble());
        }
        LinkedList<Double> FactorsTable = new LinkedList<>();
        //For-each loop required by task 2.
        FactorsArray.forEach((_item) -> {
            FactorsTable.addFirst(_item);
        });

        this.printData(CurrentController.receiveDataAndCalculeteDerivative(FactorsTable));
    }

    /**
     * This method is called by the controller to print calculated results.
     *
     * @param message String to be printed
     */
    @Override
    public void printData(String message) {
        JOptionPane.showMessageDialog(null, message, "Output", INFORMATION_MESSAGE);
    }

    /**
     * This method sets the controler that will be called to calculate
     * derivative.
     *
     * @param CurrentController Controler that will be conected to the view
     * @throws IllegalArgumentException If CurrentController is null.
     */
    @Override
    public void setController(IController CurrentController) throws IllegalArgumentException {
        if (CurrentController != null) {
            this.CurrentController = CurrentController;
        } else {
            throw new IllegalArgumentException("View's handler cannot be null.");
        }
    }

    /**
     * This is controller that will provide calculation results.
     */
    private IController CurrentController;
}
