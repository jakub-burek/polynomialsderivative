/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package view;

import controler.IController;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * This class implements view interface. It is a console I/O view.
 *
 * @version 3.0
 * @author jakub
 */
public class CConsoleView implements IView {

    /**
     * No-parameter CView constructor.
     */
    public CConsoleView() {

    }

    /**
     * This method reads data from input dialog. All non-numerical characters
     * are replaced with spaces. Scanner reads doubles from input string and
     * sends them as polynomial's factors to the controller.
     */
    @Override
    public void readData() {
        System.out.println("Type in polynomial's factors in descending order: ");
        Scanner scanner = new Scanner(System.in);
        String FactorsString = scanner.nextLine();
        FactorsString = FactorsString.replaceAll("[^-.0-9]+", " "); //"[^-?0-9]+"
        Scanner sc = new Scanner(FactorsString);
        sc.useLocale(Locale.US);
        List<Double> FactorsArray = new ArrayList<>();
        while (sc.hasNextDouble()) {
            FactorsArray.add(sc.nextDouble());
        }
        LinkedList<Double> FactorsTable = new LinkedList<>();
        //For-each loop required by task 2.
        FactorsArray.forEach((_item) -> {
            FactorsTable.addFirst(_item);
        });

        this.printData(CurrentController.receiveDataAndCalculeteDerivative(FactorsTable));
    }

    /**
     * This method is called to print calculated results.
     *
     * @param message String to be printed
     */
    @Override
    public void printData(String message) {
        System.out.println(message);
    }

    /**
     * This method sets the controler that will be called to calculate
     * derivative.
     *
     * @param CurrentController Controler that will be conected to the view
     * @throws IllegalArgumentException If CurrentController is null.
     */
    @Override
    public void setController(IController CurrentController) throws IllegalArgumentException {
        if (CurrentController != null) {
            this.CurrentController = CurrentController;
        } else {
            throw new IllegalArgumentException("View's handler cannot be null.");
        }
    }

    /**
     * This is controller that will provide calculation results.
     */
    private IController CurrentController;
}
