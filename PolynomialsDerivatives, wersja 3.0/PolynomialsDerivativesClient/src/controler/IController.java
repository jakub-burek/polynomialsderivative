/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package controler;

import TCPClient.ITCPPolynomialsClient;
import java.util.List;

/**
 * This interface can be used to implement controller.
 *
 * @version 3.0
 * @author jakub
 */
public interface IController {

    /**
     * This method receives polynomial's factor from view and calculated the
     * derivative. It then calls controller to print results.
     *
     * @param FactorsTable Polynomial's factors in descending order.
     * FactorsTable is now a safe list, not basic table.
     * @return Result data as String
     */
    public String receiveDataAndCalculeteDerivative(List<Double> FactorsTable);

    /**
     * This method sets ITCPPolynomialsClient for server communication.
     *
     * @param client Client responsible for connecting with server.
     */
    public void setITCPPolynomialsClient(ITCPPolynomialsClient client);
}
