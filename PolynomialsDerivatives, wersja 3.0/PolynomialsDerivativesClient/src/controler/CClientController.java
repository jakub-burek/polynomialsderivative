/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package controler;

import TCPClient.ITCPPolynomialsClient;
import java.util.List;

/**
 * This is controller's implementation. It can receive data and send it to
 * server via client.
 *
 * @version 3.0
 * @author jakub
 */
public class CClientController implements IController {

    /**
     * No-parameter controller constructor.
     */
    public CClientController() {
        client = null;
    }

    /**
     * In this method controller receives polynomial's factors and passes them
     * to server via client.
     *
     * @param FactorsTable Polynomial's factors in ascending order. FactorsTable
     * is now a safe list, not basic table.
     * @return String representing calculation results.
     */
    @Override
    public String receiveDataAndCalculeteDerivative(List<Double> FactorsTable) {
        if (client == null) {
            return "No client. Application not set up properly.";
        }

        String FactorsString = "";
        for (Double factor : FactorsTable) {
            FactorsString = FactorsString + factor + " ";
        }

        return client.runClient(FactorsString);
    }

    /**
     * This method is responsible for setting up controller's client.
     *
     * @param client ITCPPolynomialsClient type object.
     */
    @Override
    public void setITCPPolynomialsClient(ITCPPolynomialsClient client) {
        this.client = client;
    }

    /**
     * Reference to ITCPPolynomialsClient.
     */
    private ITCPPolynomialsClient client;
}
