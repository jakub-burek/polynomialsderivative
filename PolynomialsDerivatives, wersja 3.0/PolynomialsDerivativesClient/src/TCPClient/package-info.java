/**
 * This package contains TCP Client interface and implementation.
 * Client is responsible for communication with server.
 */
package TCPClient;
