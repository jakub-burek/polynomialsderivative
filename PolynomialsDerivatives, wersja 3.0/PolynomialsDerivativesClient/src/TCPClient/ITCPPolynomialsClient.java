/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package TCPClient;

/**
 * This interface allows to implement TCP client classes.
 *
 * @version 3.0
 * @author jakub
 */
public interface ITCPPolynomialsClient {

    /**
     * This is main ITCPPolynomialsClient method. It is responsible for creating
     * socket and input/output streams to communicate with server.
     *
     * @param data String with data (polynomials factors).
     * @return String with information or calculation output.
     */
    public String runClient(String data);

    /**
     * This method allows to set port number.
     *
     * @param port Integer port value.
     */
    public void setPort(Integer port);

    /**
     * This method allows to set address.
     *
     * @param address String address value.
     */
    public void setAddress(String address);
}
