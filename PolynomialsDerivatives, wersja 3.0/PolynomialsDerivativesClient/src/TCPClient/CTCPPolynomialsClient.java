/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package TCPClient;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * This class implements ITCPPolynomialsClient interface. It connects to server.
 *
 * @version 3.0
 * @author jakub
 */
public class CTCPPolynomialsClient implements ITCPPolynomialsClient {

    /**
     * No-parameter CTCPPolynomialsClient constructor.
     */
    public CTCPPolynomialsClient() {
        port = null;
        address = null;
    }

    /**
     * This is main CTCPPolynomialsClient method. It is responsible for creating
     * socket and input/output streams to communicate with server.
     *
     * @param data String with data (polynomials factors) in descending order.
     * @return String with information or calculation output.
     */
    @Override
    public String runClient(String data) {
        if (port == null || address == null) {
            return "Connection not set up. Cannot calculate data.";
        }

        try (Socket clientSocket = new Socket(address, port)) {
            PrintWriter outputToServer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream())), true);
            BufferedReader inputFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputFromServerString = inputFromServer.readLine();
            outputToServer.println("CALCCLIENT");
            inputFromServer.readLine();
            outputToServer.println(data);
            inputFromServerString = inputFromServer.readLine();
            outputToServer.println("QUIT");
            inputFromServer.readLine();
            return inputFromServerString;
        } catch (IOException ex) {
            return "Connection not set up. Cannot calculate data.";
        }
    }

    /**
     * This method allows to set port number.
     *
     * @param port Integer port value.
     */
    @Override
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * This method allows to set address.
     *
     * @param address String address value.
     */
    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Integer port value. Default null. Must be set.
     */
    private Integer port;

    /**
     * String address value. Default null. Must be set.
     */
    private String address;
}
