/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package polynomialsderivatives;

/**
 * This enum type helps determine what kind of IView implementation is to be
 * used. Enum values are: CONSOLE and GRAPHIC.
 *
 * @version 2.0
 * @author jakub
 */
public enum ViewType {

    CONSOLE, GRAPHIC;

    /**
     * This method checks wheather arguemnt equals appropriate string.
     *
     * @param argument String compared to appropriate phrase.
     * @return If Strings are equal it returns true, otherwise false.
     */
    public boolean sameAs(String argument) {
        switch (this) {
            case CONSOLE:
                return argument.equals("console");
            case GRAPHIC:
                return argument.equals("graphic");
            default:
                return false;
        }
    }
}
