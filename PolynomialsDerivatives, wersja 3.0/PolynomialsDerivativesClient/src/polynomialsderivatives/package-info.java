/**
 * This package contains class responsible for the entry point (main) of the application.
 * As well as enum type that is used to determine what kind of view is to be used.
 */
package polynomialsderivatives;
