/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package polynomialsderivatives;

import TCPClient.CTCPPolynomialsClient;
import TCPClient.ITCPPolynomialsClient;
import controler.CClientController;
import controler.IController;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import view.CConsoleView;
import view.CGraphicView;
import view.IView;

/**
 * Application is a TCP client, it requires properies file to set up its
 * connection with server. This application can be used to calculate
 * polynomial's derivative on server.
 *
 * @version 3.0
 * @author jakub
 */
public class PolynomialsDerivativesClient {

    /**
     * This is program's entry point it creates view and client objects. Those
     * objects are responsible for reading, printing and sending data. To run it
     * it is required to have properties.xml file with two properties: port and
     * address in it.
     *
     * @param arguments The command line argument: either "graphic" or
     * "console". If you type nothing or something wrong than nothing happens.
     * This argument allows you to choose view's version.
     */
    public static void main(String[] arguments) {
        int paramtersNumber = arguments.length;
        if (paramtersNumber != 1) {
            return;
        }

        //Enum type required by task 2, seen here in use.
        IView CurrentView = null;
        if (ViewType.CONSOLE.sameAs(arguments[0])) {
            CurrentView = new CConsoleView();
        } else if (ViewType.GRAPHIC.sameAs(arguments[0])) {
            CurrentView = new CGraphicView();
        }
        if (CurrentView == null) {
            return;
        }

        //Reading properties from xml file
        String address = null;
        Integer port = null;
        Properties properties = new Properties();
        try (FileInputStream inputFile = new FileInputStream("properties.xml")) {
            properties.loadFromXML(inputFile);
            String portString = properties.getProperty("port");
            port = Integer.parseInt(portString);
            address = properties.getProperty("address");
        } catch (IOException e) {
            return;
        }

        //Setting up client and connecting him with existing view.
        ITCPPolynomialsClient client = new CTCPPolynomialsClient();
        client.setPort(port);
        client.setAddress(address);

        //Setting up client controller and running view
        IController CurrentController = new CClientController();
        CurrentView.setController(CurrentController);
        CurrentController.setITCPPolynomialsClient(client);
        CurrentView.readData();
    }
}
