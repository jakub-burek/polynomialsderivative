/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package TCPServer;

import controler.IController;
import java.io.Closeable;
import java.io.IOException;

/**
 * This interface is used to implement services performed on the server.
 *
 * @version 3.0
 * @author jakub
 */
public interface ISinglePolynomialsService extends Closeable {

    /**
     * This is main service method. It is used to handle clients data.
     */
    public void realize();

    /**
     * Setting controller to call when calculations needed.
     *
     * @param calculationController Controller to perform calculations.
     */
    public void setController(IController calculationController);

    /**
     * This is required to allow closing server even when service is handled.
     *
     * @param thread Thread to handle server shutdown.
     */
    public void setServerShutdownThread(ServerShutdownThread thread);

    /**
     * This method can be called to clean-up after a service is complete.
     */
    @Override
    public void close() throws IOException;

}
