/**
 * This package contains TCPServer interface and implementation.
 * It also has implementation of a single service performed on the server.
 */
package TCPServer;
