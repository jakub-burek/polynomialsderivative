/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package TCPServer;

import controler.IController;
import dataModel.DegreeException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * This class implements ISignlePolynomialsService. It is used to handle client.
 *
 * @version 3.0
 * @author jakub
 */
class CSinglePolynomialsService implements Closeable, ISinglePolynomialsService {

    /**
     * This is one parameter constructor. It uses socet to create input/output
     * connection with client.
     *
     * @param socket It allows to handle connection with a client.
     * @throws IOException It is thrown if we are not able to create proper
     * stream connection with a client.
     */
    public CSinglePolynomialsService(Socket socket) throws IOException {
        this.socket = socket;
        outputWriter = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
        inputReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        calculationController = null;
        thread = null;
    }

    /**
     * This method is used to set controller to calculate data.
     *
     * @param calculationController Current controller.
     */
    @Override
    public void setController(IController calculationController) {
        if (calculationController != null) {
            this.calculationController = calculationController;
        }
    }

    /**
     * This is main service method. It allows to communicate with a client. It
     * calls controller to calculete results.
     */
    @Override
    public void realize() {
        if (calculationController == null || thread == null) {
            return;
        }
        try {
            outputWriter.println("Welcome to Polynomials Server.");
            while (true) {
                String inputString = inputReader.readLine();
                if (thread.shouldShutdown() == true) {
                    outputWriter.println("END: Server is shutting down.");
                    break;
                }
                if (inputString.toUpperCase().equals("QUIT")) {
                    outputWriter.println("END: You terminated connection.");
                    break;
                } else if (inputString.toUpperCase().equals("HELP")) {
                    outputWriter.println("HELP: Avaliable commands: QUIT, HELP, CALC.");
                } else if (inputString.toUpperCase().equals("CALC")) {
                    outputWriter.println("CALC: Now provide polynomials factors in descending order.");
                    String dataString = inputReader.readLine();
                    dataString = dataString.replaceAll("[^-.0-9]+", " ");
                    Scanner sc = new Scanner(dataString);
                    sc.useLocale(Locale.US);
                    List<Double> FactorsArray = new ArrayList<>();
                    while (sc.hasNextDouble()) {
                        FactorsArray.add(sc.nextDouble());
                    }
                    LinkedList<Double> FactorsTable = new LinkedList<>();
                    FactorsArray.forEach((_item) -> {
                        FactorsTable.addFirst(_item);
                    });
                    try {
                        if (calculationController != null) {
                            String outputData = calculationController.receiveDataAndCalculeteDerivative(FactorsTable);
                            outputWriter.println(outputData);
                        } else {
                            outputWriter.println("ERR: No data controller, server not responding.");
                        }
                    } catch (DegreeException ex) {
                        outputWriter.println("ERR: Error occured during data calculation.");
                    }
                } else if (inputString.toUpperCase().equals("CALCCLIENT")) {
                    outputWriter.println("CALCCLIENT: Now provide polynomials factors in descending order.");
                    String dataString = inputReader.readLine();
                    Scanner sc = new Scanner(dataString);
                    sc.useLocale(Locale.US);
                    List<Double> FactorsArray = new ArrayList<>();
                    while (sc.hasNextDouble()) {
                        FactorsArray.add(sc.nextDouble());
                    }
                    try {
                        if (calculationController != null) {
                            String outputData = calculationController.receiveDataAndCalculeteDerivative(FactorsArray);
                            outputWriter.println(outputData);
                        } else {
                            outputWriter.println("ERR: No data controller, server not responding.");
                        }
                    } catch (DegreeException ex) {
                        outputWriter.println("ERR: Error occured during data calculation.");
                    }
                } else {
                    outputWriter.println("ERR: Type in appropraite command.");
                }
            }
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * This is required to allow closing server even when service is handled.
     *
     * @param thread Thread to handle server shutdown.
     */
    @Override
    public void setServerShutdownThread(ServerShutdownThread thread) {
        this.thread = thread;
    }

    /**
     * This is Closeable implementation.
     *
     * @throws IOException If operation is unsuccessful.
     */
    @Override
    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
    }

    /**
     * Thread that is informing service about shutdown operation.
     */
    private ServerShutdownThread thread;

    /**
     * This socket represents client connection.
     */
    private Socket socket;

    /**
     * This reader is used to read characters from inputReader stream.
     */
    private BufferedReader inputReader;

    /**
     * This reader is used to send characters via outputWriter stream to
     * clients.
     */
    private PrintWriter outputWriter;

    /**
     * This controller is used to calculate derivative.
     */
    private IController calculationController;
}
