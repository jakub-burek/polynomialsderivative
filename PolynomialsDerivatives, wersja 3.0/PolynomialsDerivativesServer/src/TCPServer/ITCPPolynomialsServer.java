/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package TCPServer;

import controler.IController;
import java.io.Closeable;
import java.io.IOException;

/**
 * This interface can be used to implement TCP servers.
 *
 * @version 3.0
 * @author jakub
 */
public interface ITCPPolynomialsServer extends Closeable {

    /**
     * Main server method that allows to run server and services.
     *
     * @throws java.io.IOException This exception is thrown when connection is
     * lost or stream is closed.
     */
    public void runServer() throws IOException;

    /**
     * This method sets calculation controller.
     *
     * @param calculationController Reference to IController type object.
     */
    public void setController(IController calculationController);

    /**
     * This method can be called to clean-up after server's work is complete.
     */
    @Override
    public void close() throws IOException;
}
