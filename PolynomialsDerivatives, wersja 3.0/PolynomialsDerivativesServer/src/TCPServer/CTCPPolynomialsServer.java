/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package TCPServer;

import controler.IController;
import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

/**
 * This class implements ITCPPolynomialsServer to run services and Closeable to
 * close serverSocet when servieces are no longer needed.
 *
 * @version 3.0
 * @author jakub
 */
public class CTCPPolynomialsServer implements Closeable, ITCPPolynomialsServer {

    /**
     * No parameter constructor, creates server socket.
     *
     * @param port Port number as Integer.
     * @throws IOException This exception is thrown when we cannot create
     * serverSocket in our port.
     */
    public CTCPPolynomialsServer(Integer port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    /**
     * This is main server method. It provides loop that handles services when
     * our socket accepts connection. It implements ITCPPolynomialsServer
     * method.
     *
     * @throws IOException This exception is thrown when we are not able to
     * create service.
     */
    @Override
    public void runServer() throws IOException {
        ServerShutdownThread serverShutdown = new ServerShutdownThread();
        serverShutdown.start();
        serverSocket.setSoTimeout(100);
        while (true) {
            if (serverShutdown.shouldShutdown() == true) {
                break;
            }
            Socket socket = null;
            try {
                socket = serverSocket.accept();
            } catch (SocketTimeoutException ex) {
                continue;
            }

            try (ISinglePolynomialsService singleService = new CSinglePolynomialsService(socket)) {
                singleService.setServerShutdownThread(serverShutdown);
                singleService.setController(calculationController);
                singleService.realize();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * This method is used to set controller to calculate data.
     *
     * @param calculationController Current controller.
     */
    @Override
    public void setController(IController calculationController) {
        if (calculationController != null) {
            this.calculationController = calculationController;
        }
    }

    /**
     * This method implements Closeable method. It is called before server
     * instace destruction to close serverSocket.
     *
     * @throws IOException It is thrown when serverSocket connection cannot be
     * properly closed.
     */
    @Override
    public void close() throws IOException {
        if (serverSocket != null) {
            serverSocket.close();
        }
    }

    /**
     * This server socket is waiting for client connections
     */
    private ServerSocket serverSocket;

    /**
     * This controller is used to calculate derivative.
     */
    private IController calculationController;
}
