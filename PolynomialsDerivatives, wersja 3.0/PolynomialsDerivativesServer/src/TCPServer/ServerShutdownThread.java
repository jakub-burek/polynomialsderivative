/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package TCPServer;

import java.util.Scanner;

/**
 * ServerShutdownThread is a class that implements thread. It is used to
 * shutdown server.
 *
 * @version 3.0
 * @author jakub
 */
public class ServerShutdownThread extends Thread {

    /**
     * No parameter construcor
     */
    public ServerShutdownThread() {
        shouldShutdown = false;
    }

    /**
     * This method informs server, should it shutdown.
     *
     * @return True if server should shut down.
     */
    boolean shouldShutdown() {
        return shouldShutdown;
    }

    /**
     * This is main class method. It waits for server administraor to type in
     * command.
     */
    @Override
    public void run() {
        while (true) {
            System.out.println("To shutdown server type in 'quit': ");
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            if (command.contentEquals("quit")) {
                shouldShutdown = true;
                break;
            }
        }
    }

    /**
     * This field contains information about server shutdown state.
     */
    private boolean shouldShutdown;
}
