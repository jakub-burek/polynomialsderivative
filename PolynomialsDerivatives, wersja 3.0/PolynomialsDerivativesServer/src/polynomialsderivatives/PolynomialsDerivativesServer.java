/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package polynomialsderivatives;

import TCPServer.CTCPPolynomialsServer;
import TCPServer.ITCPPolynomialsServer;
import controler.CController;
import controler.IController;
import dataModel.CPolynomial;
import dataModel.IPolynomial;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Application calculates polynomial's derivative. It is a server application,
 * to read data we need to use a client application.
 *
 * @version 3.0
 * @author jakub
 */
public class PolynomialsDerivativesServer {

    /**
     * This is program's entry point it creates model and controler objects.
     * Those two objects are responsible for calculating and storing data. We
     * also create a server to handle incoming connections. We read port number
     * from properties.xml file.
     *
     * @param arguments CMD arguments, we do not need them.
     */
    public static void main(String[] arguments) {

        //Reading properties from xml file
        Integer port = null;
        Properties properties = new Properties();
        try (FileInputStream inputFile = new FileInputStream("properties.xml")) {
            properties.loadFromXML(inputFile);
            if (!properties.containsKey("port")) {
                return;
            }
            String portString = properties.getProperty("port");
            port = Integer.parseInt(portString);
        } catch (IOException e) {
            return;
        }

        IPolynomial CurrentPolynomial = new CPolynomial();
        IController CurrentController = new CController(CurrentPolynomial);
        try (ITCPPolynomialsServer CurrentServer = new CTCPPolynomialsServer(port)) {
            CurrentServer.setController(CurrentController);
            CurrentServer.runServer();
        } catch (IOException ex) {
            Logger.getLogger(PolynomialsDerivativesServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
