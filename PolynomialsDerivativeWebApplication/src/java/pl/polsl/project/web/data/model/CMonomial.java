/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.data.model;

/**
 * This class implements monomial interface. It allows you to set and get degree
 * and factor and calculate derivative.
 *
 * @version 2.0
 * @author jakub
 */
public class CMonomial implements IMonomial {

    /**
     * Two parameter CMonomial constructor.
     *
     * @param degree Monomial's degree
     * @param factor Monomial's factor
     */
    public CMonomial(int degree, double factor) {
        this.degree = degree;
        this.factor = factor;
    }

    /**
     * This method allows you to set monomial's degree.
     *
     * @param degree Monomial's degree
     */
    @Override
    public void setDegree(int degree) {
        this.degree = degree;
    }

    /**
     * This method allows you to get monomial's degree.
     *
     * @return Monomial's degree
     */
    @Override
    public int getDegree() {
        return degree;
    }

    /**
     * This method allows you to set monomial's factor.
     *
     * @param factor Monomial's factor
     */
    @Override
    public void setFactor(double factor) {
        this.factor = factor;
    }

    /**
     * This method allows you to get monomial's factor.
     *
     * @return Monomial's factor
     */
    @Override
    public double getFactor() {
        return factor;
    }

    /**
     * This method calculates monomial's derivative
     *
     * @return Returns monomials derivative
     * @throws DegreeException Is thrown if degree is inappropriate.
     */
    @Override
    public IMonomial calculateDerivative() throws DegreeException {
        if (degree < 0) {
            throw new DegreeException("Degree should not be negative.");
        }

        if (degree == 0) {
            return new CMonomial(0, 0);
        }

        int newDegree = degree - 1;
        double newFactor = factor * degree;
        return new CMonomial(newDegree, newFactor);
    }

    /**
     * Monomial's degree.
     */
    private int degree;
    /**
     * Monomial's factor.
     */
    private double factor;
}
