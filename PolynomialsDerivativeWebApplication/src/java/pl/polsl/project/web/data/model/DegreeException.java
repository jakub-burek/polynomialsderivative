/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.data.model;

/**
 * This exception is thrown when degree is not in appropriate range or is
 * negative.
 *
 * @version 1.0
 * @author jakub
 */
public class DegreeException extends Exception {

    /**
     * No parameter constructor.
     */
    DegreeException() {

    }

    /**
     * One paramter canstructor.
     *
     * @param string message describing exception
     */
    DegreeException(String string) {
        super(string);
    }
}
