/**
 * This package contains interfaces and implementations of data model.
 * Data model consists of polynomials that consists of monomials.
 * This package also contains DegreeException implementation.
 */
package pl.polsl.project.web.data.model;
