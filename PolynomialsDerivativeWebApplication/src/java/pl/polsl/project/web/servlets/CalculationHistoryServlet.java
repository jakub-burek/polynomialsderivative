/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This servlet is responsible for history of calculation.
 *
 * @version 4.0
 * @author jakub
 */
@WebServlet(name = "CalculationHistoryServlet", urlPatterns = {"/CalculationHistoryServlet"})
public class CalculationHistoryServlet extends HttpServlet {

    /**
     * This method is called to initialize servlet.
     *
     * @param config Servlet configuration data.
     * @throws ServletException If servlet error occurs.
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        history = HistorySingleton.getInstance();
    }

    /**
     * This method is responsible for printing response html code.
     *
     * @param response Handle to current servlet response.
     * @param message String that will be printed in the html response
     * @throws IOException If an I/O error occurs.
     */
    private void printResponse(HttpServletResponse response, String message) throws IOException {
        if (message == null) {
            message = "";
        }

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>CalculationHistoryServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Calculation history:</h1>");
            out.println(message);
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession currentSession = request.getSession(false);
        String status = (String) currentSession.getAttribute("state");

        if (status == null || !status.equals("logged")) {
            printResponse(response, "You must be logged in to see history.");
            return;
        }

        printResponse(response, history.getHistory());
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * History used to keep previous calculations.
     */
    private HistorySingleton history;
}
