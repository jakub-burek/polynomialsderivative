/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import pl.polsl.project.web.users.CUserDataBase;
import pl.polsl.project.web.users.IUserDataBase;

/**
 * This servlet is responsible for managing login/logout operations.
 *
 * @version 4.0
 * @author jakub
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    /**
     * This method is responsible for printing response html code.
     *
     * @param response Handle to current servlet response.
     * @param message String that will be printed in the html response
     * @throws IOException If an I/O error occurs.
     */
    private void printResponse(HttpServletResponse response, String message) throws IOException {
        if (message == null) {
            message = "";
        }

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Message:</h1>");
            out.println(message);
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession currentSession = request.getSession();
        String actionType = request.getParameter("action");
        String state = (String) currentSession.getAttribute("state");
        String login = request.getParameter("login");
        String password = request.getParameter("password");

        if (state == null) {
            state = "begining";
        }

        if (actionType == null) {
            printResponse(response, "Action failure");
            return;
        } else if (actionType.equals("login")) {
            if (login == null || password == null || login.equals("") || login.equals("") || state == null || state.equals("logged")) {
                printResponse(response, "Login failed");
                return;
            } else {
                if (dataBase.validateUser(login, password)) {
                    printResponse(response, "Login successfull.");
                    currentSession.setAttribute("state", "logged");
                    return;
                } else {
                    printResponse(response, "Login failed, wrong credentials.");
                    return;
                }
            }
        } else if (actionType.equals("register")) {
            if (login == null || password == null || login.equals("") || login.equals("") || state == null || state.equals("logged")) {
                printResponse(response, "Registration failed.");
                return;
            } else {
                if (dataBase.registerUser(login, password)) {
                    printResponse(response, "Regisration successfull.");
                    return;
                } else {
                    printResponse(response, "Registration failed, user exsist or wrong data.");
                    return;
                }
            }
        } else if (actionType.equals("delete")) {
            if (login == null || password == null || login.equals("") || login.equals("") || state == null || state.equals("logged")) {
                printResponse(response, "Deletion failed.");
                return;
            } else {
                if (dataBase.deleteUser(login, password)) {
                    printResponse(response, "Deletion successfull.");
                    return;
                } else {
                    printResponse(response, "Deletion failed, user doesn't exsist or wrong data.");
                    return;
                }
            }
        } else if (actionType.equals("logout")) {
            if (state == null || !state.equals("logged")) {
                printResponse(response, "Logout failed.");
                return;
            } else {
                currentSession.setAttribute("state", "logged out");
                printResponse(response, "Logged out.");
                return;
            }
        } else if (actionType.equals("calculate")) {
            if (state == null || !state.equals("logged")) {
                printResponse(response, "Log in to calculate.");
                return;
            } else {
                response.setStatus(HttpServletResponse.SC_FOUND);
                response.setHeader("Location", "derivative.jsp");
            }
        } else {
            printResponse(response, "Action failure");
            return;
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * This servlet uses user database to login/logout/register/delete accounts.
     */
    private IUserDataBase dataBase = new CUserDataBase();
}
