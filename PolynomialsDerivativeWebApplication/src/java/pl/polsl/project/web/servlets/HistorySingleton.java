/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.servlets;

/**
 * This signleton allows storing historic data.
 *
 * @version 4.0
 * @author jakub
 */
public class HistorySingleton {

    /**
     * This private constructor can be called only by class itself.
     */
    private HistorySingleton() {
    }

    /**
     * This method allows users to get instance of singleton.
     *
     * @return Only singleton's instance.
     */
    public static HistorySingleton getInstance() {
        if (singleInstance == null) {
            singleInstance = new HistorySingleton();
        }
        return singleInstance;
    }

    /**
     * Add a string to history.
     *
     * @param addition New string.
     */
    public void addToHistory(String addition) {
        if (addition != null) {
            history = history + addition;
        }
    }

    /**
     * Get history string.
     *
     * @return History string.
     */
    public String getHistory() {
        return history;
    }

    /**
     * History string, prepared for html print.
     */
    private String history = "";

    /**
     * This is reference to singleton.
     */
    private static HistorySingleton singleInstance = null;
}
