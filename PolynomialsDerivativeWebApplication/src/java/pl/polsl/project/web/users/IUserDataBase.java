/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.users;

/**
 * This interface enables creating user database implementation.
 *
 * @version 4.0
 * @author jakub
 */
public interface IUserDataBase {

    /**
     * This method registers user in database.
     *
     * @param login User's login.
     * @param password User's password.
     * @return True - if successfully registerd, otherwise false.
     */
    public boolean registerUser(String login, String password);

    /**
     * This method validares user's credentials in database.
     *
     * @param login User's login.
     * @param password User's password.
     * @return True - if user gave correct password to existing account,
     * otherwise - false.
     */
    public boolean validateUser(String login, String password);

    /**
     * This method deltes user's account from database.
     *
     * @param login User's login.
     * @param password User's password.
     * @return True - if account is deleted, otherwise - false.
     */
    public boolean deleteUser(String login, String password);
}
