/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.users;

/**
 * This interface enables creating user account implementation.
 *
 * @version 4.0
 * @author jakub
 */
public interface IUser {

    /**
     * This method returns user's login.
     *
     * @return User's login.
     */
    public String getLogin();

    /**
     * This method returns user's password.
     *
     * @return User's password.
     */
    public String getPassword();

    /**
     * This method allows to set user's login.
     *
     * @param login New user's login.
     */
    public void setLogin(String login);

    /**
     * This method allows to set user's password.
     *
     * @param password New user's password.
     */
    public void setPassword(String password);
}
