/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.data.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.Test;

/**
 * Class to test CMonomial class. This class has only one method that is not:
 * setter, getter or constructor. This class tests CalculateDerivative method.
 *
 * @version 2.0
 * @author jakub
 */
public class CMonomialTest {

    /**
     * This method provides CMonomial to be tested.
     */
    @Before
    public void calculateDerivativeTestPreparation() {
        testedMonomial = new CMonomial(0, 0);
    }

    /**
     * This method tests calculateDerivative method form CMonomial class.
     */
    @Test
    public void testCalculateDerivative() {

        //Inappropriate degree
        testedMonomial.setDegree(-50);
        for (int j = -1; j <= 1; j++) {
            testedMonomial.setFactor(j);
            try {
                testedMonomial.calculateDerivative();
                fail("Degree is negative should have thrown exception.");
            } catch (DegreeException ex) {
            }
        }

        //Inappropriate degree
        testedMonomial.setDegree(-1);
        for (int j = -1; j <= 1; j++) {
            testedMonomial.setFactor(j);
            try {
                testedMonomial.calculateDerivative();
                fail("Degree is negative should have thrown exception.");
            } catch (DegreeException ex) {
            }
        }

        //Appropriate degree, border value
        testedMonomial.setDegree(0);
        for (int j = -1; j <= 1; j++) {
            testedMonomial.setFactor(j);
            try {
                IMonomial derivative = testedMonomial.calculateDerivative();
                assertEquals("Degree comparison", derivative.getDegree(), 0);
                assertEquals("Factor comparison", derivative.getFactor(), 0, 0.0000001);
            } catch (DegreeException ex) {
                fail("Degree is 0, should not throw exception.");
            }
        }

        //Appropriate degree
        testedMonomial.setDegree(1);
        for (int j = -1; j <= 1; j++) {
            testedMonomial.setFactor(j);
            try {
                IMonomial derivative = testedMonomial.calculateDerivative();
                assertEquals("Degree comparison", derivative.getDegree(), 0);
                assertEquals("Factor comparison", derivative.getFactor(), j, 0.0000001);
            } catch (DegreeException ex) {
                fail("Degree is 1, should not throw exception.");
            }
        }

        //Appropriate degree
        testedMonomial.setDegree(50);
        testedMonomial.setFactor(100);
        try {
            IMonomial derivative = testedMonomial.calculateDerivative();
            assertEquals("Degree comparison", derivative.getDegree(), testedMonomial.getDegree() - 1);
            assertEquals("Factor comparison", derivative.getFactor(), testedMonomial.getDegree() * testedMonomial.getFactor(), 0.0000001);
        } catch (DegreeException ex) {
            fail("Degree is positive, should not throw exception.");
        }
    }

    /**
     * CMonomial instance that will be used in tests.
     */
    private CMonomial testedMonomial;
}
