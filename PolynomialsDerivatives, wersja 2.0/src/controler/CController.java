/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package controler;

import dataModel.IPolynomial;
import dataModel.DegreeException;
import java.util.List;

/**
 * This is controller's implementation. It can receive data and calculate
 * derivative.
 *
 * @version 2.0
 * @author jakub
 */
public class CController implements IController {

    /**
     * Controller's constructor.
     *
     * @param CurrentData Model provided to Controller.
     * @throws IllegalArgumentException If CurrentData is null.
     */
    public CController(IPolynomial CurrentData) {
        if (CurrentData == null) {
            throw new IllegalArgumentException("Data cannot be null");
        }
        this.CurrentData = CurrentData;
    }

    /**
     * In this method controller receives polynomial's factors, it uses it to
     * create polynomial, calculte its derivatice and calls view to print the
     * results.
     *
     * @param FactorsTable Polynomial's factors in descending order.
     * FactorsTable is now a safe list, not basic table.
     * @return String representing calculation results.
     * @throws dataModel.DegreeException If degree is inappropriate.
     */
    @Override
    public String receiveDataAndCalculeteDerivative(List<Double> FactorsTable) throws DegreeException {
        if (FactorsTable.size() <= 0) {
            return "Calculation unsuccessful. No appropriate data found.";
        }

        int PolynomialDegree = FactorsTable.size() - 1;
        CurrentData.setDegree(PolynomialDegree);

        for (int i = 0; i <= CurrentData.getDegree(); i++) {
            CurrentData.setFactorOfDegree(i, FactorsTable.get(i));
        }

        IPolynomial Derivative = CurrentData.CalculateDerivative();

        String polynomialAsString = this.polynomialToString(CurrentData);
        String derivativeAsString = this.polynomialToString(Derivative);
        String result = "Input: " + polynomialAsString + " Output: " + derivativeAsString;

        return result;
    }

    /**
     * This method creates string representing polynomial.
     *
     * @param polynomial Polynomial to be transformed into string.
     * @return String representing polynomial.
     * @throws DegreeException If degree is inappropriate.
     */
    private String polynomialToString(IPolynomial polynomial) throws DegreeException {
        if (polynomial == null || polynomial.getDegree() < 0) {
            return "";
        }
        String result = "";
        for (int i = polynomial.getDegree(); i > 0; i--) {
            result = result + polynomial.calculateFactorOfDegree(i) + "x^" + i + " + ";
        }
        result = result + polynomial.calculateFactorOfDegree(0);
        return result;
    }

    /**
     * Here we store current polynomial.
     */
    private IPolynomial CurrentData;
}
