/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package polynomialsderivatives;

import view.CGraphicView;
import view.IView;
import controler.CController;
import controler.IController;
import dataModel.CPolynomial;
import dataModel.IPolynomial;
import view.CConsoleView;

/**
 * Application calculates polynomial's derivative.
 *
 * @version 2.0
 * @author jakub
 */
public class PolynomialsDerivatives {

    /**
     * This is program's entry point it creates MVC objects. Those three objects
     * are responsible for reading, calculating and printing data.
     *
     * @param arguments The command line arguments: either "graphic" or
     * "console". If you type nothing or something wrong than nothing happens.
     * This argument allows you to choose view's version.
     */
    public static void main(String[] arguments) {
        int paramtersNumber = arguments.length;
        if (paramtersNumber != 1) {
            return;
        }

        //Enum type required by task 2, seen here in use.
        IView CurrentView = null;
        if (ViewType.CONSOLE.sameAs(arguments[0])) {
            CurrentView = new CConsoleView();
        } else if (ViewType.GRAPHIC.sameAs(arguments[0])) {
            CurrentView = new CGraphicView();
        }
        if (CurrentView == null) {
            return;
        }

        IPolynomial CurrentPolynomial = new CPolynomial();
        IController CurrentController = new CController(CurrentPolynomial);

        try {
            CurrentView.setController(CurrentController);
        } catch (IllegalArgumentException exception) {
            System.err.println(exception.toString());
        }

        CurrentView.readData();
    }
}
