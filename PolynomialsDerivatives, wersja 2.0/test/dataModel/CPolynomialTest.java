/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package dataModel;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import org.junit.Test;

/**
 * This class tests CPolynomial class. Only two methods that are not
 * getters/setters/constructors are: calculateFactorOfDegree,
 * calculateDerivative.
 *
 * @version 2.0
 * @author jakub
 */
public class CPolynomialTest {

    /**
     * This method tests calculateFactorOfDegree method from class CPolynomial.
     */
    @Test
    public void testCalculateFactorOfDegree() {

        //Appropriate degree.
        testedPolynomial = new CPolynomial(10);
        try {
            double factor = testedPolynomial.calculateFactorOfDegree(5);
            assertThat(factor, is(equalTo(0.0)));
        } catch (DegreeException ex) {
            fail("Appropriate degree should not have thrown exception.");
        }

        //Border value
        try {
            double factor = testedPolynomial.calculateFactorOfDegree(0);
            assertThat(factor, is(equalTo(0.0)));
        } catch (DegreeException ex) {
            fail("Appropriate degree should not have thrown exception.");
        }

        //Border value
        try {
            double factor = testedPolynomial.calculateFactorOfDegree(10);
            assertThat(factor, is(equalTo(0.0)));
        } catch (DegreeException ex) {
            fail("Appropriate degree should not have thrown exception.");
        }

        //Proper value
        testedPolynomial.setFactorOfDegree(6, 10);
        try {
            double factor = testedPolynomial.calculateFactorOfDegree(6);
            assertThat(factor, is(equalTo(10.0)));
        } catch (DegreeException ex) {
            fail("Appropriate degree should not have thrown exception.");
        }

        //Degree out of range
        try {
            testedPolynomial.calculateFactorOfDegree(11);
            fail("Inapropriate degree should have thrown exception.");
        } catch (DegreeException ex) {

        }

        //Degree out of range
        try {
            testedPolynomial.calculateFactorOfDegree(-1);
            fail("Inapropriate degree should have thrown exception.");
        } catch (DegreeException ex) {

        }

        //Null pointer exception check
        testedPolynomial = new CPolynomial(0);
        testedPolynomial.setDegree(-1);
        try {
            testedPolynomial.calculateFactorOfDegree(5);
            fail("Polynomial is null should have thrown exception.");
        } catch (DegreeException | NullPointerException ex) {

        }

        //Null pointer exception check
        testedPolynomial.setDegree(-5);
        try {
            testedPolynomial.calculateFactorOfDegree(5);
            fail("Polynomial is null should have thrown exception.");
        } catch (DegreeException | NullPointerException ex) {

        }
    }

    /**
     * This method tests calculateDerivative method from class CPolynomial.
     */
    @Test
    public void testCalculateDerivative() {

        //Appropriate data
        testedPolynomial = new CPolynomial(2);
        testedPolynomial.setFactorOfDegree(0, 5);
        testedPolynomial.setFactorOfDegree(1, 10);
        testedPolynomial.setFactorOfDegree(2, 15);
        try {
            IPolynomial derivative = testedPolynomial.CalculateDerivative();
            assertThat(derivative.calculateFactorOfDegree(0), is(equalTo(10.0)));
            assertThat(derivative.calculateFactorOfDegree(1), is(equalTo(30.0)));
        } catch (DegreeException ex) {
            fail("Valid data provided, should not have thrown exception");
        }

        //Appropraite data
        testedPolynomial = new CPolynomial(1);
        testedPolynomial.setFactorOfDegree(0, 5);
        testedPolynomial.setFactorOfDegree(1, 10);
        try {
            IPolynomial derivative = testedPolynomial.CalculateDerivative();
            assertThat(derivative.calculateFactorOfDegree(0), is(equalTo(10.0)));
        } catch (DegreeException ex) {
            fail("Valid data provided, should not have thrown exception");
        }

        //Border value
        testedPolynomial = new CPolynomial(0);
        testedPolynomial.setFactorOfDegree(0, 5);
        try {
            IPolynomial derivative = testedPolynomial.CalculateDerivative();
            assertThat(derivative.calculateFactorOfDegree(0), is(equalTo(0.0)));
        } catch (DegreeException ex) {
            fail("Valid data provided, should not have thrown exception");
        }

        //Degree out of range
        testedPolynomial = new CPolynomial(-1);
        testedPolynomial.setFactorOfDegree(-1, 5);
        try {
            IPolynomial derivative = testedPolynomial.CalculateDerivative();
            fail("Invalid data provided, should have thrown exception");
        } catch (DegreeException | NullPointerException ex) {

        }

        //Degree out of range
        testedPolynomial = new CPolynomial(-5);
        try {
            IPolynomial derivative = testedPolynomial.CalculateDerivative();
            fail("Invalid data provided, should have thrown exception");
        } catch (DegreeException | NullPointerException ex) {

        }

        //When no degree provided, Polynomial inside is null.
        testedPolynomial = new CPolynomial();
        try {
            testedPolynomial.CalculateDerivative();
            fail("Degree is not set, we cannot calculate derivative.");
        } catch (DegreeException | NullPointerException ex) {

        }

    }

    /**
     * Polynomial that will be tested.
     */
    private CPolynomial testedPolynomial;
}
