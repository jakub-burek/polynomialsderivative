/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package dataModel;

/**
 * This interface can be used to implement polynomials.
 *
 * @version 1.0
 * @author jakub
 */
public interface IPolynomial {

    /**
     * Set factor of degree. If you call this method you will wipe polynomial
     * and set new degree.
     *
     * @param degree New degree, must be positive.
     * @throws DegreeException dataModel.DegreeException If the degree is not in
     * an appropriate range.
     */
    public void setDegree(int degree) throws DegreeException;
    
    /**
     * This method allows to chceck polynomial's degree.
     *
     * @return polynomial's degree as int
     */
    public int getDegree();

    /**
     * It allows to set factor of polynomial's ingredient of given degree.
     *
     * @param degree Degree of ingredient that is supposed to have new factor.
     * @param factor New value of ingredient's factor
     * @throws dataModel.DegreeException If the degree is not in an appropriate
     * range.
     */
    public void setFactorOfDegree(int degree, double factor) throws DegreeException;

    /**
     * It returns the value of ingredient's factor of certain degree.
     *
     * @param degree Degree of the ingredient's factor
     * @return Factor of the ingredient
     * @throws dataModel.DegreeException If the degree is not in an appropriate
     * range.
     */
    public double getFactorOfDegree(int degree) throws DegreeException;

    /**
     * It returns derivative polynomial of self.
     *
     * @return Derivative polynomial of self.
     * @throws dataModel.DegreeException If the degree is not in an appropriate
     * range.
     */
    public IPolynomial CalculateDerivative() throws DegreeException;
}
