/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package dataModel;

import java.util.ArrayList;
import java.util.List;

/**
 * It implements IPolynomial interface. You can change initial value of
 * polynomial's degree. You can calculate derivative of given polynomial.
 *
 * @version 1.0
 * @author jakub
 */
public class CPolynomial implements IPolynomial {

    /**
     * No parameter CPolynomail constructor.
     */
    public CPolynomial() {
        polynomial = null;
    }

    /**
     * One parameter CPolynomial constructor. You can create polynomial of
     * certain degree. It initializes factors of degrees with zeros.
     *
     * @param degree Degree of polynomial being created.
     * @throws dataModel.DegreeException If the degree is not in an appropriate
     * range.
     */
    public CPolynomial(int degree) throws DegreeException {
        if (degree < 0) {
            throw new DegreeException("Degree must be positive.");
        }
        polynomial = new ArrayList<>(degree + 1);
        for (int i = 0; i <= degree; i++) {
            polynomial.add(i, new CMonomial(i, 0));
        }
    }

    /**
     * Set factor of degree. If you call this method you will wipe polynomial
     * and set new degree.
     *
     * @param degree New degree, must be positive.
     * @throws dataModel.DegreeException If the degree is not in an appropriate
     * range.
     */
    @Override
    public void setDegree(int degree) throws DegreeException {
        if (degree < 0) {
            throw new DegreeException("Degree must be positive.");
        }
        polynomial = new ArrayList<>(degree + 1);
        for (int i = 0; i <= degree; i++) {
            polynomial.add(i, new CMonomial(i, 0));
        }
    }

    /**
     * It returns the value of polynomial's degree.
     *
     * @return Value of polynomial's degree as int.
     */
    @Override
    public int getDegree() {
        return polynomial.size() - 1;
    }

    /**
     * It allows to set factor of polynomial's ingredient of given degree.
     *
     * @param degree Degree of ingredient that is supposed to have new factor.
     * @param factor New value of ingredient's factor
     * @throws dataModel.DegreeException If the degree is not in an appropriate
     * range.
     */
    @Override
    public void setFactorOfDegree(int degree, double factor) throws DegreeException {
        if (degree >= 0 && degree < polynomial.size()) {
            polynomial.get(degree).setFactor(factor);
        } else {
            throw new DegreeException("Degree must be in range");
        }
    }

    /**
     * It returns the value of ingredient's factor of certain degree.
     *
     * @param degree Degree of the ingredient's factor
     * @return Factor of the ingredient
     * @throws dataModel.DegreeException If the degree is not in an appropriate
     * range.
     */
    @Override
    public double getFactorOfDegree(int degree) throws DegreeException {
        if (degree >= 0 & degree < polynomial.size()) {
            return polynomial.get(degree).getFactor();
        } else {
            throw new DegreeException("Degree must be in range");
        }
    }

    /**
     * It returns derivative polynomial of self.
     *
     * @return Derivative polynomial of self.
     */
    @Override
    public IPolynomial CalculateDerivative() throws DegreeException {

        if (this.getDegree() == 0) {
            IPolynomial DerivativePolynomial = new CPolynomial(this.getDegree());
            DerivativePolynomial.setFactorOfDegree(0, 0);
            return DerivativePolynomial;
        }

        IPolynomial DerivativePolynomial = new CPolynomial(this.getDegree() - 1);

        for (int i = 1; i <= this.getDegree(); ++i) {
            DerivativePolynomial.setFactorOfDegree(i - 1, i * polynomial.get(i).getFactor());
        }

        return DerivativePolynomial;
    }

    /**
     * This list contains IMonomial-type objects that are polynomial's ingredients.
     */
    private List<IMonomial> polynomial;
}
