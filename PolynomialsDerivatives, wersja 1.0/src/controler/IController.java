/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package controler;

import dataModel.DegreeException;

/**
 * This interface can be used to implement controller.
 *
 * @version 1.0
 * @author jakub
 */
public interface IController {

    /**
     * This method receives polynomial's factor from view and calculated the
     * derivative. It then calls controller to print results.
     *
     * @param FactorsTable Polynomial's factors in descending order
     * @return Result data as String
     * @throws dataModel.DegreeException If degree is inappropriate.
     */
    public String receiveDataAndCalculeteDerivative(double FactorsTable[]) throws DegreeException;
}
