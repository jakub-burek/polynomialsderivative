/**
 * This package contains controler.
 * Implementation uses interface.
 * Controler receives data, calculates derivative and calls view to print the results.
 */
package controler;
