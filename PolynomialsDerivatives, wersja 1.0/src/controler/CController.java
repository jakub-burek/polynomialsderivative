/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package controler;

import dataModel.IPolynomial;
import dataModel.DegreeException;

/**
 * This is controller's implementation. It can receive data and calculate
 * derivative.
 *
 * @version 1.0
 * @author jakub
 */
public class CController implements IController {

    /**
     * Controller's constructor.
     *
     * @param CurrentData Model provided to Controller.
     *  @throws IllegalArgumentException If CurrentData is null.
     */
    public CController(IPolynomial CurrentData) {
        if(CurrentData == null)
            throw new IllegalArgumentException("Data cannot be null");
        this.CurrentData = CurrentData;
    }

    /**
     * In this method controller receives polynomial's factors, it uses it to
     * create polynomial, calculte its derivatice and calls view to print the
     * results.
     *
     * @param FactorsTable Polynomial's factors in descending order.
     * @return String representing calculation results.
     * @throws dataModel.DegreeException If degree is inappropriate.
     */
    @Override
    public String receiveDataAndCalculeteDerivative(double FactorsTable[]) throws DegreeException {
        int PolynomialDegree = FactorsTable.length - 1;
        CurrentData.setDegree(PolynomialDegree);

        for (int i = 0; i <= CurrentData.getDegree(); i++) {
            CurrentData.setFactorOfDegree(i, FactorsTable[i]);
        }

        IPolynomial Derivative = CurrentData.CalculateDerivative();

        String polynomialAsString = this.polynomialToString(CurrentData);
        String derivativeAsString = this.polynomialToString(Derivative);
        String result = "Input: " + polynomialAsString + " Output: " + derivativeAsString;

        return result;
    }

    /**
     * This method creates string representing polynomial.
     *
     * @param polynomial Polynomial to be transformed into string.
     * @return String representing polynomial.
     * @throws DegreeException If degree is inappropriate.
     */
    private String polynomialToString(IPolynomial polynomial) throws DegreeException {
        String result = "";
        for (int i = polynomial.getDegree(); i > 0; i--) {
            result = result + polynomial.getFactorOfDegree(i) + "x^" + i + " + ";
        }
        result = result + polynomial.getFactorOfDegree(0);
        return result;
    }

    /**
     * Here we store current polynomial.
     */
    private IPolynomial CurrentData;
}
