/**
 * This package contains view.
 * Implementation uses interface.
 * View can set its controler, read data from user and print calculed results.
 */
package view;
