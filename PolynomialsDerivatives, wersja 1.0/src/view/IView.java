/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package view;

import controler.IController;

/**
 * This interface can be used to implement view of the application.
 *
 * @version 1.0
 * @author jakub
 */
public interface IView {

    /**
     * This method reads data from user and calls controler to calculate
     * derivative.
     */
    public void readData();

    /**
     * This method is called by the controller to print calculated results.
     * 
     * @param message String to be printed
     */
    public void printData(String message);

    /**
     * This method sets the controler that will be called to calculate
     * derivative.
     *
     * @param CurrentController Controler that will be conected to the view
     */
    public void setController(IController CurrentController);

}
