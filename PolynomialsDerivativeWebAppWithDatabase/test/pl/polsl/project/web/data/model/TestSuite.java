/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.data.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Test suite it provides appropriate engine and unit tests classes.
 *
 * @version 2.0
 * @author jakub
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    CMonomialTest.class,
    CPolynomialTest.class,})
public class TestSuite {
}
