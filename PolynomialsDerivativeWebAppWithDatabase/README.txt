Baza danych utworzona zgodnie z instrukcjami ze strony:
https://netbeans.org/kb/docs/ide/java-db.html

Plik properties.xml z kt�rego aplikacja korzysta do utworzenia po��czenia znajduje si� w katalogu g��wnym i to jego zawarto��:
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd">
<properties>
<comment>--Configuration--</comment>
<entry key="url">jdbc:derby://localhost:1527/polynomials</entry>
<entry key="user">mathematician</entry>
<entry key="password">differential</entry>
<entry key="driver">org.apache.derby.jdbc.ClientDriver</entry>
</properties>

Plik properties.xml nale�y umie�ci� w odpowiednim folderze na dysku tak, aby aplikacja mog�a si� odpowiednio skonfigurowa�.
Dla mojego komputera (working directory aplikacji):
C:\Users\jakub\AppData\Roaming\NetBeans\8.2\config\GF_4.1.1\domain1\config

Jak wida� nale�y utworzy� baz� danych na porcie 1527 lokalnego hosta, kt�ra:
- nazywa si� polynomials
- u�ytkownik to mathematician
- a has�o to differential

To domy�lna �cie�ka instalacyjna:
C:\Program Files\glassfish-4.1.1\javadb
I lokalizacja bazy danych:
C:\Users\jakub\AppData\Roaming\NetBeans\Derby

Tabele s� tworzone w bazie przy uruchomieniu aplikacji, je�eli nie istniej�. 