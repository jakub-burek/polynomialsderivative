/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.data.model;

import java.util.ArrayList;
import java.util.List;

/**
 * It implements IPolynomial interface. You can change initial value of
 * polynomial's degree. You can calculate derivative of given polynomial.
 *
 * @version 2.0
 * @author jakub
 */
public class CPolynomial implements IPolynomial {

    /**
     * No parameter CPolynomail constructor.
     */
    public CPolynomial() {
        polynomial = null;
    }

    /**
     * One parameter CPolynomial constructor. You can create polynomial of
     * certain degree. It initializes factors of degrees with zeros.
     *
     * @param degree Degree of polynomial being created.
     */
    public CPolynomial(int degree) {
        if (degree < 0) {
            polynomial = null;
            return;
        }
        //Safe collection required by task 2.
        polynomial = new ArrayList<>(degree + 1);
        for (int i = 0; i <= degree; i++) {
            polynomial.add(i, new CMonomial(i, 0));
        }
    }

    /**
     * Set factor of degree. If you call this method you will wipe polynomial
     * and set new degree. If degree is negative polynomial will be null.
     *
     * @param degree New degree, must be positive.
     */
    @Override
    public void setDegree(int degree) {
        if (degree < 0) {
            polynomial = null;
            return;
        }
        //Safe collection required by task 2.
        polynomial = new ArrayList<>(degree + 1);
        for (int i = 0; i <= degree; i++) {
            polynomial.add(i, new CMonomial(i, 0));
        }
    }

    /**
     * It returns the value of polynomial's degree.
     *
     * @return Value of polynomial's degree as int. If polynomial is null it
     * returns -1.
     */
    @Override
    public int getDegree() {
        if (polynomial != null) {
            return polynomial.size() - 1;
        } else {
            return -1;
        }
    }

    /**
     * It allows to set factor of polynomial's ingredient of given degree.
     * Oparation is successsful only when degree is in range.
     *
     * @param degree Degree of ingredient that is supposed to have new factor.
     * @param factor New value of ingredient's factor
     */
    @Override
    public void setFactorOfDegree(int degree, double factor) {
        if (polynomial == null) {
            return;
        } else if (degree >= 0 && degree < polynomial.size()) {
            polynomial.get(degree).setFactor(factor);
        }
    }

    /**
     * It returns the value of ingredient's factor of certain degree.
     *
     * @param degree Degree of the ingredient's factor
     * @throws DegreeException If degree is inappropriate
     * @throws NullPointerException If polynomial is null
     * @return Factor of the ingredient
     */
    @Override
    public double calculateFactorOfDegree(int degree) throws DegreeException, NullPointerException {
        if (polynomial == null) {
            throw new NullPointerException("Cannot calculate on null pointer");
        }
        if (degree >= 0 & degree < polynomial.size()) {
            return polynomial.get(degree).getFactor();
        } else {
            throw new DegreeException("Degree must be in range");
        }
    }

    /**
     * It returns derivative polynomial of self.
     *
     * @return Derivative polynomial of self.
     * @throws DegreeException If degree is inapproprate
     * @throws NullPointerException If polynomial is null
     */
    @Override
    public IPolynomial CalculateDerivative() throws DegreeException, NullPointerException {
        if (polynomial == null) {
            throw new NullPointerException("Cannot calculate on null pointer");
        }

        if (this.getDegree() < 0) {
            throw new DegreeException("Polynomial is invalid. Cannot calculate derivative.");
        }

        if (this.getDegree() == 0) {
            IPolynomial DerivativePolynomial = new CPolynomial(this.getDegree());
            DerivativePolynomial.setFactorOfDegree(0, 0);
            return DerivativePolynomial;
        }

        IPolynomial DerivativePolynomial = new CPolynomial(this.getDegree() - 1);

        for (int i = 1; i <= this.getDegree(); ++i) {
            DerivativePolynomial.setFactorOfDegree(i - 1, i * polynomial.get(i).getFactor());
        }

        return DerivativePolynomial;
    }

    /**
     * This list contains IMonomial-type objects that are polynomial's
     * ingredients. It is a safe collection required in task 2.
     */
    private List<IMonomial> polynomial;
}
