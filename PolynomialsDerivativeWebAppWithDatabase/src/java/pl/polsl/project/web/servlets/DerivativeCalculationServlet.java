/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.servlets;

import pl.polsl.project.web.controler.CController;
import pl.polsl.project.web.controler.IController;
import pl.polsl.project.web.data.model.CPolynomial;
import pl.polsl.project.web.data.model.DegreeException;
import pl.polsl.project.web.data.model.IPolynomial;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * This servlet is responsible for calculating polynomials derivative.
 *
 * @version 5.0
 * @author jakub
 */
@WebServlet(name = "DerivativeCalculationServlet", urlPatterns = {"/DerivativeCalculationServlet"})
public class DerivativeCalculationServlet extends HttpServlet {

    /**
     * This method is called to initialize servlet.
     *
     * @param config Servlet configuration data.
     * @throws javax.servlet.ServletException If something unexpected happens
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        polynomial = new CPolynomial();
        controller = new CController(polynomial);
        connection = DBConnectionSingleton.getInstance();
    }

    /**
     * This method is responsible for printing response html code.
     *
     * @param response Handle to current servlet response.
     * @param message String that will be printed in the html response
     * @throws IOException If an I/O error occurs.
     */
    private void printResponse(HttpServletResponse response, String message) throws IOException {
        if (message == null) {
            message = "Calculation unsuccessfull.";
        }

        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>DerivativeCalculationServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Calculation results: </h1>");
            out.println(message);
            out.println("</body>");
            out.println("</html>");
        }
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession currentSession = request.getSession(false);
        String status = (String) currentSession.getAttribute("state");
        String actionType = request.getParameter("action");

        if (status == null || !status.equals("logged")) {
            printResponse(response, "You must be logged in to calculate.");
            return;
        }

        String FactorsString = (String) request.getParameter("factorsString");
        String message;
        LinkedList<Double> FactorsTable = new LinkedList<>();

        if (actionType.equals("addcookie")) {
            Cookie cookie = new Cookie("lastData", FactorsString);
            response.addCookie(cookie);
            printResponse(response, "Addeed data cookie.");
            return;
        } else if (actionType.equals("calculatecookie")) {
            FactorsString = null;
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("lastData")) {
                    FactorsString = cookie.getValue();
                    break;
                }
            }
        }

        if (FactorsString != null) {
            FactorsString = FactorsString.replaceAll("[^-.0-9]+", " ");
            Scanner sc = new Scanner(FactorsString);
            sc.useLocale(Locale.US);
            List<Double> FactorsArray = new ArrayList<>();
            while (sc.hasNextDouble()) {
                FactorsArray.add(sc.nextDouble());
            }
            //For-each loop required by task 2.
            FactorsArray.forEach((_item) -> {
                FactorsTable.addFirst(_item);
            });
        }

        try {
            message = controller.receiveDataAndCalculeteDerivative(FactorsTable);
        } catch (DegreeException exception) {
            message = "Calculation unsuccessfull.";
        }

        try {
            if (connection == null || connection.getConnection() == null) {
                printResponse(response, "<p>There is no database connection</p><p>Check your database connection or contact administrator. Then reopen web app.</p>");
                return;
            }
            PreparedStatement statement = connection.getConnection().prepareStatement("INSERT INTO HISTORY (DATA, US_ID) VALUES (?, ?)");
            String user = (String) currentSession.getAttribute("user");
            if (user == null) {
                user = "unknown";
            }
            statement.setString(1, message);
            statement.setString(2, user);
            statement.executeUpdate();
        } catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
            printResponse(response, "<p>SQL Exception occured.</p><p>Check your database connection or contact administrator. Then reopen web app.</p>");
            return;
        }

        message = "<p>" + message + "</p>";
        printResponse(response, message);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    /**
     * Database connection used for DB communication.
     */
    private DBConnectionSingleton connection;

    /**
     * Controller object responsible for providing calculation results, created
     * only once in server lifetime.
     */
    private IController controller;

    /**
     * Data model object, created only one in server lifetime.
     */
    private IPolynomial polynomial;
}
