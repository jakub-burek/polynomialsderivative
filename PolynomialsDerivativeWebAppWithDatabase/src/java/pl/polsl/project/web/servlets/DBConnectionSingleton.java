/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2019
 */
package pl.polsl.project.web.servlets;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class represents connection with database. It is a signleton shared
 * between servelts and it allows to open DB connection only once in application
 * lifetime.
 *
 * @version 5.0
 * @author jakub
 */
public class DBConnectionSingleton {

    /**
     * This private constructor can be called only by class itself.
     */
    private DBConnectionSingleton() {
        //Reading properties from xml file
        String url = null;
        String user = null;
        String password = null;
        String driver = null;
        Properties properties = new Properties();
        try (FileInputStream inputFile = new FileInputStream("properties.xml")) {
            properties.loadFromXML(inputFile);
            url = properties.getProperty("url");
            user = properties.getProperty("user");
            password = properties.getProperty("password");
            driver = properties.getProperty("driver");
        } catch (IOException e) {
            return;
        }

        try {
            Class.forName(driver);
        } catch (ClassNotFoundException cnfe) {
            return;
        }

        try {
            connection = DriverManager.getConnection(url, user, password);
            DatabaseMetaData metaData;
            ResultSet tables;
            Statement statement;
            metaData = connection.getMetaData();
            statement = connection.createStatement();
            tables = metaData.getTables(connection.getCatalog(), null, "USERS", null);
            if (!tables.next()) {
                statement.execute("CREATE TABLE USERS (US_ID VARCHAR(200) NOT NULL, PASSWORD VARCHAR(200) NOT NULL, PRIMARY KEY(US_ID))");
            }
            tables = metaData.getTables(connection.getCatalog(), null, "HISTORY", null);
            if (!tables.next()) {
                statement.execute("CREATE TABLE HISTORY (ID INTEGER NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), DATA VARCHAR(200) NOT NULL, US_ID VARCHAR(200), PRIMARY KEY(ID), FOREIGN KEY(US_ID) REFERENCES USERS ON DELETE SET NULL)");
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConnectionSingleton.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This method allows users to get instance of singleton.
     *
     * @return Only singleton's instance.
     */
    public static DBConnectionSingleton getInstance() {
        if (singleInstance == null) {
            singleInstance = new DBConnectionSingleton();
        }
        return singleInstance;
    }

    /**
     * This method allows to use one connection in all servlets.
     *
     * @return Connection type object, if connection not set up properly, it
     * will be null.
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * This is reference to connection field used by all servlets.
     */
    private Connection connection = null;

    /**
     * This is reference to singleton.
     */
    private static DBConnectionSingleton singleInstance = null;
}
