/**
 * This package contains IController interface and its implementation - CController.
 * Controller receives list of polynomial's factors and calculates its derivative and returns the outcome in form of a String.
 */
package pl.polsl.project.web.controler;
