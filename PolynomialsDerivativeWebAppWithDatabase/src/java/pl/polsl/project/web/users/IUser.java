/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.users;

/**
 * This interface enables creating user account implementation.
 *
 * @version 4.0
 * @author jakub
 */
@Deprecated
public interface IUser {

    /**
     * This method returns user's login.
     *
     * @return User's login.
     */
    @Deprecated
    public String getLogin();

    /**
     * This method returns user's password.
     *
     * @return User's password.
     */
    @Deprecated
    public String getPassword();

    /**
     * This method allows to set user's login.
     *
     * @param login New user's login.
     */
    @Deprecated
    public void setLogin(String login);

    /**
     * This method allows to set user's password.
     *
     * @param password New user's password.
     */
    @Deprecated
    public void setPassword(String password);
}
