/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.users;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * This is user database implementation. It allows managing accounts.
 *
 * @version 4.0
 * @author jakub
 */
@Deprecated
public class CUserDataBase implements IUserDataBase {

    /**
     * Public no parameted constructor.
     */
    @Deprecated
    public CUserDataBase() {
        userDataBase = new ArrayList<>();

        try (FileReader reader = new FileReader("data.txt")) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                String[] dataArray = line.split(" ");
                if (dataArray.length == 2) {
                    userDataBase.add(new CUser(dataArray[0], dataArray[1]));
                }
            }
        } catch (IOException e) {
        }
    }

    /**
     * This method registers user in database.
     *
     * @param login User's login.
     * @param password User's password.
     * @return True - if successfully registerd, otherwise false.
     */
    @Override
    @Deprecated
    public boolean registerUser(String login, String password) {
        if (!userDataBase.stream().noneMatch((user) -> (user.getLogin().equals(login)))) {
            return false;
        }

        try (BufferedWriter out = new BufferedWriter(new FileWriter("data.txt", true))) {
            String data = login + " " + password;
            out.write(data + System.lineSeparator());
        } catch (IOException e) {
        }

        userDataBase.add(new CUser(login, password));
        return true;
    }

    /**
     * This method validares user's credentials in database.
     *
     * @param login User's login.
     * @param password User's password.
     * @return True - if user gave correct password to existing account,
     * otherwise - false.
     */
    @Override
    @Deprecated
    public boolean validateUser(String login, String password) {
        if (userDataBase.stream().anyMatch((user) -> (user.getLogin().equals(login) && user.getPassword().equals(password)))) {
            return true;
        }
        return false;
    }

    private void deleteUserFromFile(String login, String password) {
        File inputFile = new File("data.txt");
        File tempFile = new File("tmporary.txt");

        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile)); BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile))) {
            String lineToRemove = login + " " + password;
            String currentLine;

            while ((currentLine = reader.readLine()) != null) {
                String trimmedLine = currentLine.trim();
                if (trimmedLine.equals(lineToRemove)) {
                    continue;
                }
                writer.write(currentLine + System.lineSeparator());
            }
        } catch (IOException ex) {
        }
        inputFile.delete();
        tempFile.renameTo(inputFile);
    }

    /**
     * This method deltes user's account from database.
     *
     * @param login User's login.
     * @param password User's password.
     * @return True - if account is deleted, otherwise - false.
     */
    @Override
    @Deprecated
    public boolean deleteUser(String login, String password) {
        for (IUser user : userDataBase) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                userDataBase.remove(user);
                deleteUserFromFile(login, password);
                return true;
            }
        }
        return false;
    }

    /**
     * List containg IUser type objects.
     */
    private List<IUser> userDataBase;
}
