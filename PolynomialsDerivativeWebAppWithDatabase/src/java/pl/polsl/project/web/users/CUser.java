/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package pl.polsl.project.web.users;

/**
 * This is user account implementation.
 *
 * @version 4.0
 * @author jakub
 */
@Deprecated
public class CUser implements IUser {

    /**
     * Public two-parameter constructor.
     *
     * @param login User's login.
     * @param password User's password.
     * @throws IllegalArgumentException If login or password is null.
     */
    @Deprecated
    public CUser(String login, String password) {
        if (login == null || password == null) {
            throw new IllegalArgumentException("Login/Password cannot be null");
        }
        this.login = login;
        this.password = password;
    }

    /**
     * This method returns user's login.
     *
     * @return User's login.
     */
    @Override
    @Deprecated
    public String getLogin() {
        return login;
    }

    /**
     * This method returns user's password.
     *
     * @return User's password.
     */
    @Override
    @Deprecated
    public String getPassword() {
        return password;
    }

    /**
     * This method allows to set user's login.
     *
     * @param login New user's login.
     */
    @Override
    @Deprecated
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * This method allows to set user's password.
     *
     * @param password New user's password.
     */
    @Override
    @Deprecated
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * String representing login.
     */
    private String login;

    /**
     * String representing password.
     */
    private String password;
}
