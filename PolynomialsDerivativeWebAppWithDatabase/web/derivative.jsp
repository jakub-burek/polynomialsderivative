<%-- 
    Document   : derivative
    Created on : 2018-12-04, 23:40:23
    Author     : jakub
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div>Calculation site</div>
        <form action="DerivativeCalculationServlet" method="POST">
            <p>Put in polynomial's factors in descending order: <input type=text name=factorsString></p>
            <input type="submit" name="action" value="calculate" />
            <input type="submit" name="action" value="addcookie" />
            <input type="submit" name="action" value="calculatecookie" />
        </form>
        <div>Calculation history</div>
        <form action="CalculationHistoryServlet" method="POST">
            <input type="submit" name="action" value="history" />
        </form>
        <div>Account site</div>
        <form action="LoginServlet" method="POST">
            <input type="submit" name="action" value="logout" />
        </form>
    </body>
</html>
