/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package com.example.data.model;

/**
 * This interface can be used to implement polynomials.
 *
 * @version 2.0
 * @author jakub
 */
public interface IPolynomial {

    /**
     * Set factor of degree. If you call this method you will wipe polynomial
     * and set new degree.
     *
     * @param degree New degree, must be positive.
     */
    public void setDegree(int degree);

    /**
     * This method allows to chceck polynomial's degree.
     *
     * @return polynomial's degree as int
     */
    public int getDegree();

    /**
     * It allows to set factor of polynomial's ingredient of given degree. It
     * does nothing if degree is out of range.
     *
     * @param degree Degree of ingredient that is supposed to have new factor.
     * @param factor New value of ingredient's factor
     */
    public void setFactorOfDegree(int degree, double factor);

    /**
     * It returns the value of ingredient's factor of certain degree.
     *
     * @param degree Degree of the ingredient's factor
     * @throws DegreeException If degree is inappropriate.
     * @return Factor of the ingredient
     */
    public double calculateFactorOfDegree(int degree) throws DegreeException;

    /**
     * It returns derivative polynomial of self.
     *
     * @return Derivative polynomial of self.
     * @throws com.example.data.model.DegreeException If the degree is
     * not in an appropriate range.
     */
    public IPolynomial CalculateDerivative() throws DegreeException;
}
