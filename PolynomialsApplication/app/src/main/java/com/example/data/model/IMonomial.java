/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package com.example.data.model;

/**
 * This interface can be used to implement monomials.
 *
 * @version 2.0
 * @author jakub
 */
public interface IMonomial {

    /**
     * It allows you to set monomial's degree.
     *
     * @param degree Monomial's new degree
     */
    public void setDegree(int degree);

    /**
     * It returns monomial's degree.
     *
     * @return Value of monomial's degree.
     */
    public int getDegree();

    /**
     * It allows you to set monomial's factor
     *
     * @param factor Value of monomial's factor.
     */
    public void setFactor(double factor);

    /**
     * It returns monomial's factor.
     *
     * @return Value of monomial's factor
     */
    public double getFactor();

    /**
     * It returns derivative monomial of self.
     *
     * @return Derivative polynomial of self.
     * @throws DegreeException If degree is inappropriate.
     */
    public IMonomial calculateDerivative() throws DegreeException;
}
