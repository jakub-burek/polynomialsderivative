/* 
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package com.example.controler;

import com.example.data.model.DegreeException;
import java.util.List;

/**
 * This interface can be used to implement controller.
 *
 * @version 2.0
 * @author jakub
 */
public interface IController {

    /**
     * This method receives polynomial's factor from view and calculated the
     * derivative. It then calls controller to print results.
     *
     * @param FactorsTable Polynomial's factors in descending order.
     * FactorsTable is now a safe list, not basic table.
     * @return Result data as String
     * @throws com.example.data.model.DegreeException If degree is
     * inappropriate.
     */
    public String receiveDataAndCalculeteDerivative(List<Double> FactorsTable) throws DegreeException;
}
