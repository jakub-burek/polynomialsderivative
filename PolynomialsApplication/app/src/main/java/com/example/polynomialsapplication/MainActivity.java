/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package com.example.polynomialsapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.example.controler.CController;
import com.example.controler.IController;
import com.example.data.model.CPolynomial;
import com.example.data.model.DegreeException;
import com.example.data.model.IPolynomial;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

/**
 * This is main activity, it is called when app is started.
 * It allows to calculate derivative and go to history activity.
 *
 * @author jakub
 * @version 6.0
 */
public class MainActivity extends AppCompatActivity {

    /**
     * When activity is created we set up data structures and controller to calculate derivative.
     * We also get handles to views involved.
     *
     * @param savedInstanceState Bundle not used by the app.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        polynomial = new CPolynomial();
        controller = new CController(polynomial);
        outputListView = findViewById(R.id.OutputListView);
        outputListView.setEnabled(true);
        inputTextField = findViewById(R.id.InputTextField);
        HistoryList = new ArrayList<>();
    }

    /**
     * When calculate button is clicked we get data from input field,
     * we use controller to calculate derivative, we display results and add them to history.
     *
     * @param v View concerned with clicked button.
     */
    public void OnCalculateButtonClicked(View v) {

        String FactorsString = null;
        String message;
        LinkedList<Double> FactorsTable = new LinkedList<>();

        if(inputTextField != null) {
            FactorsString = inputTextField.getText().toString();
        }

        if (FactorsString != null) {
            FactorsString = FactorsString.replaceAll("[^-.0-9]+", " ");
            Scanner sc = new Scanner(FactorsString);
            sc.useLocale(Locale.US);
            List<Double> FactorsArray = new ArrayList<>();
            while (sc.hasNextDouble()) {
                FactorsArray.add(sc.nextDouble());
            }
            //For-each loop required by task 2.
            for(Double item : FactorsArray)
            {
                FactorsTable.addFirst(item);
            }
        }

        try {
            message = controller.receiveDataAndCalculeteDerivative(FactorsTable);
        } catch (DegreeException exception) {
            message = "Calculation unsuccessfull.";
        }

        ArrayList<String> TemporaryList = new ArrayList<>();
        TemporaryList.add(message);
        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, TemporaryList);
        outputListView.setAdapter(adapter);
        HistoryList.add(message);

    }

    /**
     * When history button is clicked we create intent, attach gathered results and start history activity.
     *
     * @param v View concerned with clicked button.
     */
    public void OnHistoryButtonClicked(View v) {
        Intent intent = new Intent(this, HistoryActivity.class);
        intent.putStringArrayListExtra("HistoryList", HistoryList);
        startActivity(intent);
    }

    /**
     * Handle to output list view that holds calculation results.
     */
    ListView outputListView = null;

    /**
     * Handle to input text field.
     */
    private EditText inputTextField = null;

    /**
     * ArrayList holding calculation results.
     */
    private ArrayList<String> HistoryList;

    /**
     * Controller object responsible for providing calculation results, created
     * only once in server lifetime.
     */
    private IController controller;

    /**
     * Data model object, created only one in server lifetime.
     */
    private IPolynomial polynomial;
}
