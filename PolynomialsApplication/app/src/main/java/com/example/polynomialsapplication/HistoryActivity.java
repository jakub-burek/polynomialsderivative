/*
 * This is educational project.
 * Jakub Burek, Gliwice, 2018
 */
package com.example.polynomialsapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;

/**
 * This class is history activity, it is used to display all the calculation results that have been provided during the lifespan of application process.
 *
 * @author jakub
 * @version 6.0
 */
public class HistoryActivity extends AppCompatActivity {

    /**
     * Method called when activity is created, it gets an array list of strings provided by main activity.
     *
     * @param savedInstanceState Bundle not used by application
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        historyListView = findViewById(R.id.HistoryListView);
        historyListView.setEnabled(true);

        HistoryList = getIntent().getExtras().getStringArrayList("HistoryList");
        ArrayAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, HistoryList);
        historyListView.setAdapter(adapter);
    }

    /**
     * ListView that displays gathered data.
     */
    private ListView historyListView = null;

    /**
     * ArrayList of Strings that contains calculation history.
     */
    private ArrayList<String> HistoryList;
}
